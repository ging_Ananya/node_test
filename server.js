var http = require('http')
var port = process.env.PORT || 1337;
var express = require('express');
var app = express();
var path = require('path');
var users = require('./users');
var bodyParser = require('body-parser');

// http.createServer(function(req, res) {
//   res.writeHead(200, { 'Content-Type': 'text/plain' });
//   res.end('Test Hello World\n');
// }).listen(port);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

 app.get('/index', function (req, res) {
    res.send('<h1>This is index page</h1>');
 });

var server =  app.listen(port, function() {
     console.log('Starting node.js on port ' + port);
});

 app.get('/', function (req, res) {
    res.send('<h1>Hello Node.js</h1>');
});

 app.get('/user', function (req, res) {
    res.json(users.findAll());
 });

 app.get('/user/:id', function (req, res) {
     var id = req.params.id;
     res.json(users.findById(id));
 });

 app.post('/newuser', function (req, res) {
    var json = req.body;
    res.send('Add new ' + json.name + ' Completed!');
});
 
 app.set('views', path.join(__dirname, 'views'));
 app.set('view engine', 'jade'); 
 app.use(express.static('public'));
 
 
 app.get('/chat', function(req, res) {
     res.render('chat');
 });
 
 var io = require('socket.io').listen(server);
 
 io.on('connection', function(socket) {
    // เมื่อได้รับข้อมูลจากท่อ "chat" ให้ทำอะไร?
    socket.on('chat', function(message) {
        // แสดงข้อมูลที่ได้ ออกมาทาง console
         io.emit('chat', message);
        console.log(message);
     });
 });