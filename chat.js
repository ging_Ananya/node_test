var express = require('express');
var app = express();
var path = require('path');
var http = require('http')
var port = process.env.PORT || 1337;
 
var server = app.listen(port, function() {
    console.log('Listening on port: ' + port);
  	res.end('Ging Hello World\n');
}); 
 
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade'); 
app.use(express.static('public'));
 
app.get('/', function(req, res) {
    res.render('chat');
});

var io = require('socket.io').listen(server);
 
io.on('connection', function(socket) {
    // เมื่อได้รับข้อมูลจากท่อ "chat" ให้ทำอะไร?
    socket.on('chat', function(message) {
        // แสดงข้อมูลที่ได้ ออกมาทาง console
         io.emit('chat', message);
        console.log(message);
    });
});